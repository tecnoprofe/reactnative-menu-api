import {Component} from "react";
import { 
    Text, 
    View,        
    ScrollView,
    StatusBar,
    StyleSheet,
    TouchableHighlight,
    Alert
} from 'react-native';
import axios from "axios";

import {Stack, FormControl, WarningOutlineIcon, NativeBaseProvider, Box,HStack,VStack,Checkbox,Input,Button } from "native-base";

export default class Home extends Component{
  constructor(props) {
    super(props);

    this.state = {
      data: [],      
      email: "",
      password: "",
      licencia: false
    };
  }

  async postLogin() {    
    
    try {
      const response = await axios.post('http://tecnoprofe.com/api/loginapp',
      {email:this.state.email,password:this.state.password.toString()});
      this.setState({data:response.data});
    }catch (error) {
      console.error(error);
    }
    finally {
      this.setState({ isLoading: false });
    }

    if(this.state.data=="error"){
      alert("Error")
      this.setState({licencia:true})
    }
    else{     
      this.setState({licencia:false})
      
    }
}



    createTwoButtonAlert = () =>
    Alert.alert(
      "Alert Title",
      "My Alert Msg",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log("OK Pressed") }
      ]
    );

    render() {
        return (                 
          <NativeBaseProvider>  
                      
        
            

            <Box alignItems="center">
              <Box w="100%" maxWidth="300px">
                <FormControl isRequired>
                  <Stack mx="4">
                    <Text>{this.state.email}</Text>
                    <FormControl.Label>Email</FormControl.Label>
                      <Input type="email" 
                      placeholder="email" 
                      onChangeText={(cad)=>{this.setState({email:cad})}}/>                      
                    <FormControl.HelperText>
                      Must be atleast 6 characters.
                    </FormControl.HelperText>
                    
                    <FormControl.Label>Password</FormControl.Label>
                    <Text>{this.state.password}</Text>
                      <Input type="password" 
                      placeholder="password" 
                      onChangeText={(cad)=>{this.setState({password:cad})}}/>                      
                    <FormControl.HelperText>
                      Must be atleast 6 characters.
                    </FormControl.HelperText>
                    <Button onPress={() => this.postLogin()}>LOGIN</Button>
                  </Stack>
                </FormControl>
              </Box>
            </Box>          
          </NativeBaseProvider>      
        );
    }
}
const styles = StyleSheet.create({
    container: {    
      backgroundColor: '#ff8',
      color:'white'    
    },  
});