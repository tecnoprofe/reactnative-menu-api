import {Component} from "react";
import { 
    Text, 
    View,
    StatusBar,
    StyleSheet,
    ActivityIndicator
} from 'react-native';
import { FlatList } from "react-native-gesture-handler";
import axios from 'axios';


import Framecocktail from "../components/Framecocktail";


// JSON



export default class View01 extends Component{
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: true
    };
  }

  async getSongs() {
    try {
      const response = await axios.get('https://www.thecocktaildb.com/api/json/v1/1/search.php?s=margarita');
      this.setState({data:response.data.drinks});      
    }catch (error) {
      console.error(error);
    }
    finally {
      this.setState({ isLoading: false });
    }
  }

  componentDidMount() {    
    this.getSongs();
  }

    render() {
        return (                 
            <View  style={styles.container}>
              

              {this.state.isLoading==true? <ActivityIndicator/> : (
                <FlatList 
                  data={this.state.data}
                  keyExtractor={item => item.idDrink}
                  renderItem={({item})=><Framecocktail datos={item}/>}
                />
              )}

            </View >
        );
      }
}
const styles = StyleSheet.create({
    container: {
        
      backgroundColor: '#ff8',
      color:'white'    
    },  
});
