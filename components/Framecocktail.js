import {Component} from "react";
import { 
    Text, 
    View,
    Alert,
    Button, 
    ScrollView,
    StatusBar,
    StyleSheet,
    Image
} from 'react-native';

export default class Framecocktail extends Component{
    constructor(props){
        super(props);
    };

    render() {
        return (                 
            <View  style={styles.container}>
                <View style={{flexDirection:'row'}}>                    
                    <View>
                        <Image
                        style={styles.logo}
                        source={{uri: this.props.datos.strDrinkThumb}}
                        />
                    </View>
                    <View>
                        <Text>{this.props.datos.strDrink}</Text>
                        <Text>INGREDIENTES:</Text>
                        <Text>{this.props.datos.strIngredient1}</Text>
                        <Text>{this.props.datos.strIngredient2}</Text>
                        <Text>{this.props.datos.strIngredient3}</Text>
                        <Text>{this.props.datos.strIngredient4}</Text>
                    </View>                    
                </View>
            </View >

        );
      }
}
const styles = StyleSheet.create({
    container: {    
      backgroundColor: '#ff8',
      color:'white'    
    },  
    logo: {
        width: 100,
        height: 100,
    },
});
