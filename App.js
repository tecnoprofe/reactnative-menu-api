import React, { Component } from 'react';
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from "@react-navigation/native";
import "react-native-gesture-handler";

import { NativeBaseProvider, Box,HStack,VStack,Checkbox,Input,Button } from "native-base";

import { 
  Text, 
  View,
  Alert,
  Image,  
  ScrollView,
  StatusBar,
  StyleSheet
} from 'react-native';



import Home  from "./pages/Home";
import Acercade from "./pages/Acercade";
import Getmusic  from "./pages/Getmusic";
import Postmusic  from "./pages/Postmusic";
import Imc from "./pages/Imc";
import Player from "./pages/Player";
import View01 from "./pages/View01";


import axios from 'axios';

const Menu=createDrawerNavigator();




export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],      
      email: "",
      password: "",
      licencia: false
    };
  }

  async postLogin() {    
    try {
      const response = await axios.post('http://tecnoprofe.com/api/loginapp',
      {email:this.state.email,password:this.state.password.toString()});
      this.setState({data:response.data});
    }catch (error) {
      console.error(error);
    }
    finally {
      this.setState({ isLoading: false });
    }
    if(this.state.data!="error"){
      this.setState({licencia:true})
    }
    else{
      this.setState({licencia:false})
    }
  }


  render() {
    return (
      
    
      <NavigationContainer>

        <Menu.Navigator>                  
          
          <Menu.Screen name="ghfghfhfhgfh" component={Home}/>
          <Menu.Screen name="Traer Musica" component={Getmusic}/>          
          <Menu.Screen name="Registrar Musica" component={Postmusic}/>
          <Menu.Screen name="Acerca de mi" component={Acercade}/>
          <Menu.Screen name="IMC" component={Imc}/>
          <Menu.Screen name="Player" component={Player}/>
          <Menu.Screen name="View Example" component={View01}/>
        
        </Menu.Navigator>
      </NavigationContainer>        
    
      
    );
  }
}
